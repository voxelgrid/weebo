#pragma once

namespace cleanflight {

/// Enumeration of the supported MSP command messages.
enum class MspCommand : uint8_t {
	RawImu       = 102,
	Motor        = 104,
	Attitude     = 108,
	CalibrateAcc = 205,
	CalibrateMag = 206,
};

/// Header for outgoing messages.
constexpr char MSP_OUT_HEADER[3] = { '$', 'M', '<' };

/// Header for incoming messages.
constexpr char MSP_INP_HEADER[3] = { '$', 'M', '>' };

/// Represents a reply to the RawImu command.
struct MspReplyRawImu {
	int16_t acc[3];
	int16_t gyr[3];
	int16_t mag[3];
};

/// Represents a reply to the Status command.
struct MspReplyMotor {
	uint16_t motor[16];
};

/// Represents a reply to the Attitude command.
struct MspReplyAttitude {
	int16_t angX;
	int16_t angY;
	int16_t heading;
};

}
