#pragma once

#include <cstdint>
#include <memory>

namespace cleanflight {

class CleanFlightController {
public:
	/**
	 * Create a CleanFlightController object from a serial port path.
	 */
	static std::unique_ptr<CleanFlightController> CreateWithSerialPortPath(
		const std::string &path);

	/**
	 * Constructor of CleanFlightController.
	 *
	 * @param fd File descriptor that must be used to communicate with
	 *           the controller. This object will take ownership of the
	 *           file descriptor and close upon destruction.
	 */
	CleanFlightController(int fd);

	/**
	 * Destructor of CleanFlightController.
	 */
	~CleanFlightController();

	/**
	 * Calibrate the accelerometer.
	 *
	 * @return
	 * True on success, false otherwise.
	 */
	bool calibrateAcc();

	/**
	 * Calibrate the magnetometer.
	 *
	 * @return
	 * True on success, false otherwise.
	 */
	bool calibrateMag();

	/**
	 * Get the controller attitude data.
	 *
	 * @param[out] angX
	 *
	 * @param[out] angY
	 *
	 * @param[out] heading
	 *
	 * @return
	 * True on success, false otherwise.
	 */
	bool getAttitude(float *angX, float *angY, float *heading);

	/**
	 * Get the controller motor data.
	 *
	 * @param[out] motor This must be an array large enough to store 16
	 *                   elements. On success, populated with the motor
	 *                   data from the controller.
	 *
	 * @return
	 * True on success, false otherwise.
	 */
	bool getMotor(uint16_t motor[16]);

	/**
	 * Get the controller raw IMU data.
	 *
	 * @param[out] acc This must be an array large enough to store three
	 *                 elements. On success, populated with the raw acc
	 *                 data from the controller.
	 *
	 * @param[out] gyr This must be an array large enough to store three
	 *                 elements. On success, populated with the raw gyr
	 *                 data from the controller.
	 *
	 * @param[out] mag This must be an array large enough to store three
	 *                 elements. On success, populated with the raw mag
	 *                 data from the controller.
	 *
	 * @return
	 * True on success, false otherwise.
	 */
	bool getRawImu(float acc[3], float gyr[3], float mag[3]);

private:
	/**
	 * Send a message to the controller (i.e. command) and wait for a reply.
	 *
	 * @param msg         Type of message sent to the controller. This must
	 *                    be a valid MSP message type.
	 *
	 * @param reply       On success, this will be populated with the reply
	 *                    received from the controller.
	 *
	 * @param replyLength Number of bytes expected as a reply. The |reply|
	 *                    argument must be large enough to store
	 *                    |replyLength| bytes.
	 *
	 * @return
	 * True on success, false otherwise.
	 */
	bool sendMessageWithReply(uint8_t msg, void *reply, uint8_t replyLength);

	/**
	 * Send a message to the controller.
	 *
	 * @param cmd           Type of the message sent to the controller. This
	 *                      must be a valid MSP message type.
	 *
	 * @param payload       Payload to be transmitted to the controller.
	 *
	 * @param payloadLength Number of bytes in the |payload| array.
	 *
	 * @return
	 * True on success, false otherwise.
	 */
	bool sendMessage(uint8_t cmd, const void *payload, uint8_t payloadLength);

	/**
	 * Wait for a reply from the controller.
	 *
	 * @param cmd         Type of the message the controller should be
	 *                    receiving. This must be a valid MSP message
	 *                    type.
	 *
	 * @param reply       On success, this will be populated with the reply
	 *                    received from the controller.
	 *
	 * @param replyLength Number of bytes expected as a reply. The |reply|
	 *                    argument must be large enough to store
	 *                    |replyLength| bytes.
	 *
	 * @return
	 * True on success, false otherwise.
	 */
	bool waitForReply(uint8_t cmd, void *reply, uint8_t replyLength);

public:
	int mFd;
};

}

