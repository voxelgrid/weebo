#include <cleanflight/CleanFlightController.hpp>
#include <cleanflight/Calibrate.h>

#include <ros/ros.h>

#include <cerrno>
#include <cstring>
#include <string>

using cleanflight::CleanFlightController;
using cleanflight::Calibrate;

// CleanFlight controller.
static std::unique_ptr<CleanFlightController> gController;

static bool
DoCalibrate(Calibrate::Request &req, Calibrate::Response &res)
{
	if (gController == nullptr) {
		ROS_ERROR("controller not initialized");
		return false;
	}

	if (!gController->calibrateAcc()) {
		ROS_ERROR("failed to calibrate the controller's accelerometer");
		return false;
	}

	if (!gController->calibrateMag()) {
		ROS_ERROR("failed to calibrate the controller's magnetometer");
		return false;
	}

	ROS_INFO("controller calibrated");

	return true;
}

int
main(int argc, char *argv[])
{
	ros::init(argc, argv, "cleanflight_node");
	ros::NodeHandle n;

	if (argc != 2) {
		ROS_ERROR("usage: cleanflight_node [com]");
		return -1;
	}

	gController = CleanFlightController::CreateWithSerialPortPath(argv[1]);
	if (gController == nullptr) {
		ROS_ERROR("could not initialize the controller");
		return -1;
	}
	ROS_INFO("controller initialized");

	// Advertise the calibration service.
	ros::ServiceServer service = n.advertiseService("calibrate",
		DoCalibrate);

	while (ros::ok()) {
		float angX, angY, heading;
		if (!gController->getAttitude(&angX, &angY, &heading)) {
			ROS_ERROR("could not get attitude data");
			continue;
		}

		ROS_DEBUG("angX=%3.3f angY=%3.3f heading=%3.3f",
			angX, angY, heading);

		ros::spinOnce();
	}

	return 0;
}

