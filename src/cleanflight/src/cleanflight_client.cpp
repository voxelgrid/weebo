#include <cleanflight/Calibrate.h>

#include <ros/ros.h>

#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <getopt.h>

static const option long_options[] = {
	{ "calibrate", no_argument, 0, 'c' },
	{ 0 },
};

static void
DoCalibrate(ros::NodeHandle &n)
{
	// Prepare client.
	ros::ServiceClient client = n.serviceClient<cleanflight::Calibrate>(
		"calibrate");
	// Prepare service request.
	cleanflight::Calibrate srv;
	// Invoke service call.
	ROS_INFO("OK");
	if (!client.call(srv)) {
		ROS_ERROR("could not calibrate the controller acc/mag");
		exit(1);
	}
}

int
main(int argc, char *argv[])
{
	ros::init(argc, argv, "cleanflight_client");
	ros::NodeHandle n;

	int c = 0;
	int option_index = 0;
	while ((c = getopt_long(argc, argv, "c", long_options, &option_index)) != -1) {
		switch (c) {
		case 'c':
			DoCalibrate(n);
			break;
		default:
			break;
		}
	}

	return 0;
}

