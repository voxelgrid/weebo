#include <cleanflight/CleanFlightController.hpp>
#include <cleanflight/MspProtocol.hpp>

#include <cassert>
#include <cerrno>
#include <cstdio>
#include <cstring>

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

namespace cleanflight {

static uint8_t
CalculateMspCrc(uint8_t cmd, const void *buf, uint8_t bufLength)
{
	const uint8_t *bufPtr = (const uint8_t *)buf;
	uint8_t crc = 0;

	for (uint8_t i = 0; i < bufLength; ++i)
		crc ^= bufPtr[i];
	crc ^= bufLength;
	crc ^= cmd;

	return crc;
}

static bool
ReadBuffer(int fd, void *buf, size_t bufLength)
{
	uint8_t *bufPtr = (uint8_t *)buf;

	assert(fd >= 0);
	assert((bufLength == 0) || (buf != nullptr));

	while (bufLength != 0) {
		ssize_t nbytes = read(fd, bufPtr, bufLength);
		if (nbytes < 0) {
			fprintf(stderr, "%s: read() failed, reason: %s\n",
				__func__, strerror(errno));
			return false;
		}

		bufPtr += nbytes;
		bufLength -= nbytes;
	}

	return true;
}

static bool
WriteBuffer(int fd, const void *buf, size_t bufLength)
{
	const uint8_t *bufPtr = (const uint8_t*)buf;

	assert(fd >= 0);
	assert((bufLength == 0) || (buf != nullptr));

	while (bufLength != 0) {
		ssize_t nbytes = write(fd, bufPtr, bufLength);
		if (nbytes < 0) {
			fprintf(stderr, "%s: write() failed, reason: %s\n",
				__func__, strerror(errno));
			return false;
		}

		bufPtr += nbytes;
		bufLength -= nbytes;
	}

	return true;
}

std::unique_ptr<CleanFlightController>
CleanFlightController::CreateWithSerialPortPath(const std::string &path)
{
	termios tty {0};
	int fd;

	if ((fd = open(path.c_str(), O_RDWR | O_NOCTTY | O_SYNC)) < 0) {
		fprintf(stderr, "%s: open() failed, reason: %s\n",
			__func__, strerror(errno));
		goto fail;
	}

	if (tcgetattr(fd, &tty) != 0) {
		fprintf(stderr, "%s: tcgetattr() failed: %s\n",
			__func__, strerror(errno));
		goto fail;
	}

	if (cfsetospeed(&tty, B115200) != 0) {
		fprintf(stderr, "%s: cfsetospeed() failed: %s\n",
			__func__, strerror(errno));
		goto fail;
	}

	if (cfsetispeed(&tty, B115200) != 0) {
		fprintf(stderr, "%s: cfsetispeed() failed: %s\n",
			__func__, strerror(errno));
		goto fail;
	}

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
	tty.c_iflag &= ~IGNBRK;
	tty.c_lflag = 0;
	tty.c_oflag = 0;
	tty.c_cc[VMIN]  = 0;
	tty.c_cc[VTIME] = 5;
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
	tty.c_cflag |= (CLOCAL | CREAD);
	tty.c_cflag &= ~(PARENB | PARODD);
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		fprintf(stderr, "%s: tcsetattr() failed: %s\n",
			__func__, strerror(errno));
		goto fail;
	}

	tcflush (fd, TCOFLUSH);
	return std::make_unique<CleanFlightController>(fd);
fail:
	if (fd >= 0)
		(void)close(fd);
	return nullptr;
}

CleanFlightController::CleanFlightController(int fd)
	: mFd(fd)
{
	assert(fd >= 0);
}

CleanFlightController::~CleanFlightController()
{
	if (mFd) {
		(void)close(mFd);
	}
}

bool
CleanFlightController::calibrateAcc()
{
	bool success = sendMessageWithReply((uint8_t)MspCommand::CalibrateAcc,
		nullptr, 0);
	if (!success) {
		return false;
	}

	return true;
}

bool
CleanFlightController::calibrateMag()
{
	bool success = sendMessageWithReply((uint8_t)MspCommand::CalibrateMag,
		nullptr, 0);
	if (!success) {
		return false;
	}

	return true;
}

bool
CleanFlightController::getAttitude(float *angX, float *angY, float *heading)
{
	MspReplyAttitude reply;

	assert(angX);
	assert(angY);
	assert(heading);

	bool success = sendMessageWithReply((uint8_t)MspCommand::Attitude,
		(uint8_t*)&reply, sizeof(reply));
	if (!success) {
		return false;
	}

	*angX = reply.angX / 10.f;
	*angY = reply.angY / 10.f;
	*heading = (float)reply.heading;

	return true;
}

bool
CleanFlightController::getMotor(uint16_t motor[16])
{
	MspReplyMotor reply;

	bool success = sendMessageWithReply((uint8_t)MspCommand::Motor,
		(uint8_t*)&reply, sizeof(reply));
	if (!success) {
		return false;
	}

	(void)memcpy(motor, reply.motor, sizeof(reply.motor));

	return true;

}

bool
CleanFlightController::getRawImu(float acc[3], float gyr[3], float mag[3])
{
	MspReplyRawImu reply;

	bool success = sendMessageWithReply((uint8_t)MspCommand::RawImu,
		(uint8_t*)&reply, sizeof(reply));
	if (!success) {
		return false;
	}

	for (size_t i = 0; i < 3; ++i) {
		acc[i] = (float)reply.acc[i];
		gyr[i] = (float)reply.gyr[i];
		mag[i] = (float)reply.mag[i];
	}

	return true;
}

bool
CleanFlightController::sendMessageWithReply(uint8_t msg, void *reply,
	uint8_t replyLength)
{
	assert((replyLength == 0) || (reply != nullptr));

	if (!sendMessage(msg, nullptr, 0)) {
		fprintf(stderr, "%s: could not send message '%u'\n", __func__, msg);
		return false;
	}

	if (!waitForReply(msg, reply, replyLength)) {
		fprintf(stderr, "%s: could not wait for a reply to message '%u'\n",
			__func__, msg);
		return false;
	}

	return true;
}

bool
CleanFlightController::sendMessage(uint8_t cmd, const void *payload, uint8_t payloadLength)
{
	assert((payloadLength == 0) || (payload != nullptr));

	if (!WriteBuffer(mFd, MSP_OUT_HEADER, sizeof(MSP_OUT_HEADER))) {
		fprintf(stderr, "%s: could not write header\n", __func__);
		return false;
	}

	if (!WriteBuffer(mFd, &payloadLength, sizeof(payloadLength))) {
		fprintf(stderr, "%s: could not write payload length\n", __func__);
		return false;
	}

	if (!WriteBuffer(mFd, &cmd, sizeof(cmd))) {
		fprintf(stderr, "%s: could not write command\n", __func__);
		return false;
	}

	if (!WriteBuffer(mFd, payload, payloadLength)) {
		fprintf(stderr, "%s: could not write payload\n", __func__);
		return false;
	}

	uint8_t crc = CalculateMspCrc(cmd, payload, payloadLength);
	if (!WriteBuffer(mFd, &crc, sizeof(crc))) {
		fprintf(stderr, "%s: could not write CRC\n", __func__);
		return false;
	}

	return true;
}

bool
CleanFlightController::waitForReply(uint8_t cmd, void *reply, uint8_t replyLength)
{
	char readHeader[4] = {0};
	uint8_t readCmd;
	uint8_t readCrc;
	uint8_t readReplyLength;

	assert((replyLength == 0) || (reply != nullptr));

	if (!ReadBuffer(mFd, readHeader, sizeof(MSP_INP_HEADER))) {
		fprintf(stderr, "%s: could not read header\n", __func__);
		return false;
	} else if (strncmp(readHeader, MSP_INP_HEADER, sizeof(MSP_INP_HEADER))) {
		fprintf(stderr, "%s: read mismatched header ('%s')\n",
			__func__, readHeader);
		return false;
	}

	if (!ReadBuffer(mFd, &readReplyLength, sizeof(readReplyLength))) {
		fprintf(stderr, "%s: could not read reply length\n", __func__);
		return false;
	} else if (readReplyLength != replyLength) {
		fprintf(stderr, "%s: read mismatched reply length (%u != %u)\n",
			__func__, readReplyLength, replyLength);
		return false;
	}

	if (!ReadBuffer(mFd, &readCmd, sizeof(readCmd))) {
		fprintf(stderr, "%s: could not read reply command\n", __func__);
		return false;
	} else if (readCmd != cmd) {
		fprintf(stderr, "%s: read mismatched command (%u != %u)\n",
			__func__, readCmd, readCmd);
		return false;
	}

	if (!ReadBuffer(mFd, reply, replyLength)) {
		fprintf(stderr, "%s: could not read reply\n", __func__);
		return false;
	}

	if (!ReadBuffer(mFd, &readCrc, sizeof(readCrc))) {
		fprintf(stderr, "%s: could not read reply crc\n", __func__);
		return false;
	}

	return true;
}

}

